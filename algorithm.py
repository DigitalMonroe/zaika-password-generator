"""Main algorithm of password generation"""

from random import randint


def genpass_random(from_number=16, to_number=64):
    """Generats random password in random radio"""
    letters = 'qwertyuiopasdfghjklzxcvbnm'
    numbers = '1234567890'
    symbols = '!@#$%^&()_?'
    finish_pass = ''

    letters_len = len(letters) - 1
    numbers_len = len(numbers) - 1
    symbols_len = len(symbols) - 1

    password_len = randint(from_number, to_number)

    symbols_gen_len = randint(4, int(password_len / 4))
    numbers_gen_len = randint(4, int(password_len / 4))
    up_letters_gen_len = randint(4, int(password_len / 4))

    sum_elements = symbols_gen_len + numbers_gen_len + up_letters_gen_len

    letters_gen_len = password_len - sum_elements

    letters_gen_len += password_len - (sum_elements + letters_gen_len)

    letters_count = 0
    numbers_count = 0
    symbols_count = 0
    upletters_count = 0

    is_letters_full = False
    is_numbers_full = False
    is_symbols_full = False
    is_upletters_full = False

    choise_list = [1, 2, 3, 4]
    choise = 0

    i = 1
    while i > 0:
        if len(choise_list) > 0:
            choise = choise_list[randint(0, len(choise_list)-1)]

        #Letters generation
        if choise == 1 and letters_count < letters_gen_len:
            finish_pass += letters[randint(0, letters_len)]
            letters_count += 1
        elif letters_count >= letters_gen_len and not is_letters_full:
            choise_list.remove(1)
            is_letters_full = True

        #Numbers generation
        if choise == 2 and numbers_count < numbers_gen_len:
            finish_pass += numbers[randint(0, numbers_len)]
            numbers_count += 1
        elif numbers_count >= numbers_gen_len and not is_numbers_full:
            choise_list.remove(2)
            is_numbers_full = True

        #Symbols generation
        if choise == 3 and symbols_count < symbols_gen_len:
            finish_pass += symbols[randint(0, symbols_len)]
            symbols_count += 1
        elif symbols_count >= symbols_gen_len and not is_symbols_full:
            choise_list.remove(3)
            is_symbols_full = True

        #UpLetters generation
        if choise == 4 and upletters_count < up_letters_gen_len:
            finish_pass += letters[randint(0, letters_len)].upper()
            upletters_count += 1
        elif upletters_count >= up_letters_gen_len and not is_upletters_full:
            choise_list.remove(4)
            is_upletters_full = True

        i = len(choise_list)

    return finish_pass


def genpass_custom(
                    numbers_gen_len=4, letters_gen_len=4,
                    symbols_gen_len=4, up_letters_gen_len=4
                ):
    """Generats random password in custom radio"""
    letters = 'qwertyuiopasdfghjklzxcvbnm'
    numbers = '1234567890'
    symbols = '!@#$%^&()_?'
    finish_pass = ''

    letters_len = len(letters) - 1
    numbers_len = len(numbers) - 1
    symbols_len = len(symbols) - 1

    letters_count = 0
    numbers_count = 0
    symbols_count = 0
    upletters_count = 0

    is_letters_full = False
    is_numbers_full = False
    is_symbols_full = False
    is_upletters_full = False

    choise_list = [1, 2, 3, 4]
    choise = 0

    i = 1
    while i > 0:
        if len(choise_list) > 0:
            choise = choise_list[randint(0, len(choise_list)-1)]

        #Letters generation
        if choise == 1 and letters_count < letters_gen_len:
            finish_pass += letters[randint(0, letters_len)]
            letters_count += 1
        elif letters_count >= letters_gen_len and not is_letters_full:
            choise_list.remove(1)
            is_letters_full = True

        #Numbers generation
        if choise == 2 and numbers_count < numbers_gen_len:
            finish_pass += numbers[randint(0, numbers_len)]
            numbers_count += 1
        elif numbers_count >= numbers_gen_len and not is_numbers_full:
            choise_list.remove(2)
            is_numbers_full = True

        #Symbols generation
        if choise == 3 and symbols_count < symbols_gen_len:
            finish_pass += symbols[randint(0, symbols_len)]
            symbols_count += 1
        elif symbols_count >= symbols_gen_len and not is_symbols_full:
            choise_list.remove(3)
            is_symbols_full = True

        #UpLetters generation
        if choise == 4 and upletters_count < up_letters_gen_len:
            finish_pass += letters[randint(0, letters_len)].upper()
            upletters_count += 1
        elif upletters_count >= up_letters_gen_len and not is_upletters_full:
            choise_list.remove(4)
            is_upletters_full = True

        i = len(choise_list)

    return finish_pass
